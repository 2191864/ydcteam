import java.util.Random;
import java.util.Scanner;

/**
 * This program simulates the single channel queuing system for the course 9336 CS 323 Modeling and Simulation.
 *
 * @author Cris Lawrence Sidoro
 * @author Dajen Torralba
 * @author Yzza Mae Ventura
 */
public class TeamYDC {
    static  Random rand = new Random(); // instance of Random class

    public static void main(String[] args) {
        boolean startSimulation = true;
        int[] previousData;
        while (startSimulation) {
            System.out.println("\n===========================================");
            System.out.println("SIMULATION OF SINGLE CHANNEL QUEUING SYSTEM");
            System.out.println("===========================================");

            Scanner scan = new Scanner(System.in); // instance of Scanner class

            System.out.print("\nInput the Number of Customers to Start the Simulation: ");
            int customers = scan.nextInt(); // assigns user input to customers, a variable that stores the number of customers

            int totalInterArrTime;
            int totalServiceTime;
            int totalWaitingTime;
            int totalSpentTimeInSystem;
            int totalServerIdleTime;

            int customersWhoWaited = 0;
            int simulationTime = 0;

            // formats the header of the table to be generated
            System.out.println("\nSIMULATION TABLE");
            System.out.println("+=============================================================================================================================================================+");
            System.out.printf("%s%20s%15s%15s%20s%15s%20s%20s%24s", "| Customer", "Interarrival", "Arrival", "Service", "Time Service", "Waiting", "Time Service", "Time Customer", "Idle Time        |");
            System.out.printf("%s%21s%20s%15s%10s%18s%15s%31s%21s", "\n| Number", " Time (Mins)", "Time (Mins)", "Time (Mins)", " Begins", "Time", "Ends", "Spends in System", "of Server (Mins) |");
            System.out.println("\n+=============================================================================================================================================================+");

            int firstTimeServiceBegins = computeTimeServiceBegins(0, 0);
            int firstServiceTime = computeServiceTime();
            int firstTimeServiceEnds = computeTimeServiceEnds(firstServiceTime, firstTimeServiceBegins);
            int[] first = {1, 0, 0, firstServiceTime, firstTimeServiceBegins, computeWaitingTime(firstTimeServiceBegins, 0), firstTimeServiceEnds, computeTimeCustomerSpendsSystem(firstTimeServiceEnds, 0), 0};
            System.out.printf("%-18s%-20d%-15d%-15d%-20d%-15d%-19d%-20d%-3d%14s%n", "| " + first[0], first[1], first[2], first[3], first[4], first[5], first[6], first[7], first[8], "|");

            previousData = first;

            totalInterArrTime = previousData[1];
            totalServiceTime = previousData[3];
            totalWaitingTime = previousData[5];
            totalSpentTimeInSystem = previousData[7];
            totalServerIdleTime = previousData[8];

            // generates the table for single channel queuing system
            for (int customerNumber = 2; customerNumber <= customers; customerNumber++) {
                customerNumber = previousData[0] + 1;

                int currentInterArrTime = computeInterarrivalTime();
                totalInterArrTime += currentInterArrTime;

                int currentArrTime = computeArrivalTime(currentInterArrTime, previousData[2]);

                int currentServiceTime = computeServiceTime();
                totalServiceTime += currentServiceTime;

                int currentTimeServiceBegins = computeTimeServiceBegins(currentArrTime, previousData[6]);

                int currentWaitingTime = computeWaitingTime(currentTimeServiceBegins, currentArrTime);
                totalWaitingTime += currentWaitingTime;
                if (currentWaitingTime > 0)
                    customersWhoWaited++;

                int currentTimeServiceEnds = computeTimeServiceEnds(currentServiceTime, currentTimeServiceBegins);
                simulationTime = currentTimeServiceEnds;

                int currentTimeCustomerSpends = computeTimeCustomerSpendsSystem(currentTimeServiceEnds, currentArrTime);
                totalSpentTimeInSystem += currentTimeCustomerSpends;

                int currentIdleTime = computeIdleTime(currentTimeServiceBegins, previousData[6]);
                totalServerIdleTime += currentIdleTime;

                System.out.printf("%-18s%-20d%-15d%-15d%-20d%-15d%-19d%-20d%-3d%14s%n", "| " + customerNumber, currentInterArrTime, currentArrTime, currentServiceTime, currentTimeServiceBegins, currentWaitingTime, currentTimeServiceEnds, currentTimeCustomerSpends, currentIdleTime, "|");

                previousData = new int[]{customerNumber, currentInterArrTime, currentArrTime, currentServiceTime, currentTimeServiceBegins, currentWaitingTime, currentTimeServiceEnds, currentTimeCustomerSpends, currentIdleTime};
            }

            System.out.println("+-------------------------------------------------------------------------------------------------------------------------------------------------------------+");
            System.out.printf("%-18s%-35d%-35d%-34d%-20d%-16d%-1s%n", "| TOTAL:", totalInterArrTime, totalServiceTime, totalWaitingTime, totalSpentTimeInSystem, totalServerIdleTime, "|");
            System.out.println("+=============================================================================================================================================================+");

            System.out.println("\nThe Following Are Determined Based on the Generated Simulation Table:");
            double averageWaitingTime = (double) totalWaitingTime / customers;
            System.out.println("\nAVERAGE WAITING TIME = TOTAL TIME CUSTOMERS WAIT IN QUEUE (MINS)/TOTAL NUMBER OF CUSTOMERS");
            System.out.println("Total Time Customers Waited in Queue (Mins): " + totalWaitingTime);
            System.out.println("Total Number of Customers: " + customers);
            System.out.println("Average Waiting Time = " + totalWaitingTime + "/" + customers);
            System.out.println("Average Waiting Time = " + averageWaitingTime);

            double waitingProbability = (double) customersWhoWaited / customers;
            System.out.println("\nPROBABILITY THAT A CUSTOMER HAS TO WAIT IN THE QUEUE = NUMBER OF CUSTOMERS WHO WAIT/ TOTAL NUMBER OF CUSTOMERS");
            System.out.println("Number of Customers Who Waited: " + customersWhoWaited);
            System.out.println("Total Number of Customers: " + customers);
            System.out.println("Probability of Such = " + customersWhoWaited + "/" + customers);
            System.out.println("Probability of Such = " + waitingProbability);

            double serverIdleTimeProportion = (double) totalServerIdleTime / simulationTime;
            System.out.println("\nPROPORTION OF IDLE TIME OF THE SERVER = TOTAL TIME THE SERVER WAS IDLE/SIMULATION TIME");
            System.out.println("Total Time the Server Was Idle: " + totalServerIdleTime);
            System.out.println("Simulation Time: " + simulationTime);
            System.out.println("Proportion of Such = " + totalServerIdleTime + "/" + simulationTime);
            System.out.println("Proportion of Such = " + serverIdleTimeProportion);

            double averageServiceTime = (double) totalServiceTime / customers;
            System.out.println("\nAVERAGE SERVICE TIME = TOTAL SERVICE TIME/TOTAL NUMBER OF CUSTOMERS");
            System.out.println("Total Service Time: " + totalServiceTime);
            System.out.println("Total Number of Customers: " + customers);
            System.out.println("Average Service Time = " + totalServiceTime + "/" + customers);
            System.out.println("Average Service Time = " + averageServiceTime);

            double averageInterArrTime = (double) totalInterArrTime / (customers-1);
            System.out.println("\nAVERAGE TIME BETWEEN ARRIVALS = SUM OF ALL TIMES BETWEEN ARRIVAL / NUMBER OF ARRIVALS – 1");
            System.out.println("Sum of All Times Between Arrival: " + totalInterArrTime);
            System.out.println("Number of Arrivals: " + customers);
            System.out.println("Average Interarrival Time = " + totalInterArrTime + " / (" + customers + "-1)");
            System.out.println("Average Interarrival Time = " + averageInterArrTime);

            double averageWaitingTimeWaitedInQueue = (double) totalWaitingTime / customersWhoWaited;
            System.out.println("\nAVERAGE WAITING TIME = TOTAL TIME CUSTOMERS WAIT IN QUEUE (MINS)/TOTAL NUMBER OF CUSTOMERS WHO WAITED");
            System.out.println("Total Time Customers Waited in Queue (Mins): " + totalWaitingTime);
            System.out.println("Total Number of Customers Who Waited: " + customersWhoWaited);
            System.out.println("Average Waiting Time Queue = " + totalWaitingTime + "/" + customersWhoWaited);
            System.out.println("Average Waiting Time in Queue = " + averageWaitingTimeWaitedInQueue);

            double timeSpentInSystem = averageWaitingTime + averageServiceTime;
            System.out.println("\nAVERAGE TIME A CUSTOMER SPENDS IN THE SYSTEM = AVERAGE TIME CUSTOMER SPENDS WAITING IN THE QUEUE + AVERAGE TIME CUSTOMER SPENDS IN SERVICE");
            System.out.println("Average Time customer spends waiting in the queue: " + averageWaitingTime);
            System.out.println("Average Time customer spends in service: " + averageServiceTime);
            System.out.println("Average time in the system = " + averageWaitingTime + "+" + averageServiceTime);
            System.out.println("Average time in the system = " + timeSpentInSystem);

            System.out.print("\nWould you like to rerun the simulation? (Press N for No and any key for Yes): ");
            String decision = scan.next();
            if (decision.equalsIgnoreCase("n")) {
                startSimulation = false;
                System.out.println("\nGood Bye and Have a Nice Day!");
            }
        }
    }

    public static int computeInterarrivalTime(){
        int random;
        random = (1 + rand.nextInt(999));

        if (random <= 124) {
            random = 1;
        } else if (random <= 249) {
            random = 2;
        } else if (random <= 374) {
            random = 3;
        } else if (random <= 499) {
            random = 4;
        } else if (random <= 624) {
            random = 5;
        } else if (random <= 749) {
            random = 6;
        } else if (random <= 874) {
            random = 7;
        } else {
            random = 8;
        }

        return random;

    }

    public static int computeArrivalTime(int currentInterArrivalTime,int prevArrivalTime){
        return currentInterArrivalTime + prevArrivalTime;
    }

    public static int computeServiceTime(){
        int random;
        random = (1 + rand.nextInt(99));

        if (random <= 9) {
            random = 1;
        } else if (random <= 29) {
            random = 2;
        } else if (random <= 59) {
            random = 3;
        } else if (random <= 84) {
            random = 4;
        } else if (random <= 94) {
            random = 5;
        } else if (random <= 99) {
            random = 6;
        }
        return random;
    }

    public static int computeTimeServiceBegins(int currentArrivalTime,int prevTimeServiceEnds){
        return Math.max(currentArrivalTime, prevTimeServiceEnds);
    }

    public static int computeWaitingTime(int currentTimeServiceBegins,int currentArrivalTime){
        return currentTimeServiceBegins-currentArrivalTime;
    }

    public static int computeTimeServiceEnds(int currentServiceTime, int currentTimeServiceBegins){
        return currentServiceTime + currentTimeServiceBegins;
    }

    public static int computeTimeCustomerSpendsSystem(int currentTimeServiceEnds, int currentArrivalTime){
        return currentTimeServiceEnds - currentArrivalTime;
    }

    public static int computeIdleTime(int currentTimeServiceBegins, int previousTimeServiceEnds){
        return currentTimeServiceBegins - previousTimeServiceEnds;
    }


}